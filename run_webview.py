#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
if __name__ == '__main__':
  if not os.path.exists('./_layouts'):
    raise OSError('the _layouts directory doesnt exist! clone again pls :)')
  try:
    os.system('cd _layouts && yarn')
  except OSError:
    raise("""
      *for your own sanity just install yarn:*
      `npm i -g yarn@latest`
    """)
  os.system('cd _layouts && yarn && yarn dev')
