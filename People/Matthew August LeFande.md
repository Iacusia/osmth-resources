# Matthew August LeFande

> PhoneNumber (Unknown)

> Email (Unknown)

## Significance

One of the founders of [[Sigma Scorpii International]]

## Relations

Friend of [[Robert Disney]]

## Address

Unknown

[//begin]: # "Autogenerated link references for markdown compatibility"
[Robert Disney]: <Robert Disney.md> "Robert Disney"
[//end]: # "Autogenerated link references"