from shodan import Shodan as Shd
from shodan.cli.helpers import get_api_key as Apikey

if __name__ == '__main__':
  api = Shd(Apikey())

  limit = 500
  counter = 0
  for banner in api.search_cursor('product:mongodb'):
      # Perform some custom manipulations or stream the results to a database
      # For this example, I'll just print out the "data" property
      print(banner['data'])
      
      # Keep track of how many results have been downloaded so we don't use up all our query credits
      counter += 1
      if counter >= limit:
          break

