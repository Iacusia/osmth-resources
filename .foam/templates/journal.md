---
foam_template:
  filepath: 'journal/$FOAM_TITLE.md'
---


# ${FOAM_TITLE}

${2: Entry}

-------------
**Created:: ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DAY}**
