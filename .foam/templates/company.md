---
foam_template:
  filepath: 'Companies/$FOAM_TITLE.md'
---

# ${1:$FOAM_TITLE}

## Relations

## Owners

## Offices & Branches

-----------

## References

[^1] ${2:Ref}

**Created :: $FOAM_DATE_YEAR-$FOAM_DATE_MONTH-$FOAM_DATE_DAY**
