---
foam_template:
  filepath: 'People/$FOAM_TITLE.md'
---

# ${1:$FOAM_TITLE}

> ${2:PhoneNumber}

> ${3:Email}

## ${4:Significance}

## ${5:Relations}

## ${5:Address}

```log
  ${6:AddressInfo}
```

## ${6:OtherInfo}

-------------
**Created:: $FOAM_DATE_YEAR-$FOAM_DATE_MONTH-$FOAM_DATE_DAY**