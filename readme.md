# OSMTH | Sovereign Military Order of the Temple of Jerusalem

![banner](./assets/banner.png)

Ordo Supremus Militaris Templi Hierosolymitani
Swiss Federal Registry Number CH-660.1.972.999-4

## Background

After the revealing of the Larmenius Charter in `1841`, the OSMTH organisation was formed.
It's been widely supposed that the charter was a *forgery* of the original document written in `1324`.

> [Larmenius Charter](<https://en.m.wikipedia.org/wiki/Larmenius_Charter>)

It's unsure what they were doing for the last 200 years,
but they came back with a *wordpress* website registered in  `1999`.

Member registration requires providing a CV, and must fill out the requirements detailed in the
*[Decleration of Principles](<https://osmth.org/images/documents/public/1841_Declaration_of_Principles.pdf>)*.
