# OSMTH dot org

Registered in the name [[Robert Disney]]

```log
Domain: osmth.org
Registrar: Launchpad.com Inc.
Registered On: 1999-02-16

Expires On: 2023-02-16
Updated On: 2022-02-01

Status: clientTransferProhibited

Name Servers:
  ns8475.hostgator.com
  ns8476.hostgator.com

Organization: SMOTJ
State: California
Country: US
```

![whois lookup info](./whoislookup.png)

*__Why the fuck is the next registrant SMOTJ??__*
